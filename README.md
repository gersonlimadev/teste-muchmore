# Teste Much More

Esse projeto usa [NodeJS](http://nodejs.org/), [Gulp](http://gulpjs.com/) e [Stylus](http://learnboost.github.io/stylus/).

Configurando ambiente
----------

1. Abra o `Terminal`
2. Navegue até a pasta do projeto `cd pasta_do_projeto`
3. Com o nodejs instalado, instale as dependêcias do projeto `npm install`
4. E para compilar os arquivos `gulp watch`.
5. Acesse o server criado, provalvelmente `http://localhost:5000`