module.exports = {
    src: './src',
    dist: './public',
    html: {
        src: './src',
        dist: './public'
    },
    images: {
        src: './src/assets/img',
        dist: './public/assets/img'
    },
    styles: {
        src: './src/assets/styl',
        dist: './public/assets/css'
    },
    scripts: {
        src: './src/assets/js',
        dist: './public/assets/js'
    },
    vendors: {
        src: './src/assets/vendors',
    }
}