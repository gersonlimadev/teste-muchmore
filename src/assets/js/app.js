//  __  __       _          _
// |  \/  | __ _(_)_ __    / \   _ __  _ __
// | |\/| |/ _` | | '_ \  / _ \ | '_ \| '_ \
// | |  | | (_| | | | | |/ ___ \| |_) | |_) |
// |_|  |_|\__,_|_|_| |_/_/   \_\ .__/| .__/
//                              |_|   |_|

(function(){
	'use strict';
	
	angular.module('MainApp', [
		'ngRoute',
		'MainApp.Controllers', 
		'MainApp.Directives', 
		'MainApp.Services', 
		'MainApp.Filters'
	])
	.config(['$routeProvider', function ($routeProvider) {
		
		$routeProvider
			.when('/', {
				templateUrl: CONFIG.baseUrl + '/home-template.html',
				controller: 'HomeController',
				controllerAs: 'home'
			})
			.when('/single/:id', {
				templateUrl: CONFIG.baseUrl + '/single-template.html',
				controller: 'SingleController',
				controllerAs: 'single'
			})
			.otherwise({ redirectTo: '/' })

	}]);
	
})();



