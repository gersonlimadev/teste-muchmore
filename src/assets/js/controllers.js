//   ____            _             _ _
//  / ___|___  _ __ | |_ _ __ ___ | | | ___ _ __ ___
// | |   / _ \| '_ \| __| '__/ _ \| | |/ _ \ '__/ __|
// | |__| (_) | | | | |_| | | (_) | | |  __/ |  \__ \
//  \____\___/|_| |_|\__|_|  \___/|_|_|\___|_|  |___/

(function(){
	'use strict';


	angular.module('MainApp.Controllers', [])
		.controller('MainController', MainController)
		.controller('HomeController', HomeController)
		.controller('SingleController', SingleController)


	function MainController() {
		var scope = this;
		// TODO something global here...
	}
		
	HomeController.$inject = ['$filter', 'ApiService'];
	function HomeController($filter, ApiService) {
		var scope = this;
		
		ApiService.getData().then(function(response) {
			var filteredData = $filter('renderData')(response.data);
			scope.filtered = filteredData;
		})

	}
	
	SingleController.$inject = ['$routeParams', '$filter', 'ApiService'];
	function SingleController($routeParams, $filter, ApiService) {
		var scope = this;
		
		ApiService.getData().then(function(response) {
			var filteredData = $filter('renderData')(response.data);
			var search = $filter('filter')(filteredData.data, { id: $routeParams.id });
			if(search.length) {
				scope.data = filteredData.data;
				scope.post = search[0];
			}
		})
	}

})();