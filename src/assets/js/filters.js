//  _____ _ _ _
// |  ___(_) | |_ ___ _ __ ___
// | |_  | | | __/ _ \ '__/ __|
// |  _| | | | ||  __/ |  \__ \
// |_|   |_|_|\__\___|_|  |___/

(function(){
	'use strict';
	
	angular.module('MainApp.Filters', [])
		.filter('limitText', limitText)
		.filter('trustAsHtml', trustAsHtml)
		.filter('renderData', renderData)
		.filter('filterData', filterData)
		.filter('filterDataByGenre', filterDataByGenre)



		function limitText() {
			return function(text, limit) {
				var t = text.substr(0, limit);
				
				// evita quebrar palavra
				t = t.substr(0, Math.min(t.length, t.lastIndexOf(" ")));

				return t + '...';
			}
		}

		trustAsHtml.$inject = ['$sce'];
		function trustAsHtml($sce) {
			return function(txt) {
				return $sce.trustAsHtml(txt);
			}
		}

		renderData.$inject = ['$filter'];
		function renderData($filter) {
			return function(data) {
				
				var filteredData = data;
				var genres = [];
				var hours = [];

				angular.forEach(data, function(item, index) {
					var itemGenres = item.genre.split(',');
					var itemHours = [];
					var sessionsByDate = {};
					var iframeVideo = item.trailer ? '<iframe width="560" height="315" src="'+ item.trailer +'?rel=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>' : '';
					

					// generos
					angular.forEach(itemGenres, function(genre) {
						if(genres.indexOf(genre.trim()) < 0) {
							genres.push(genre.trim());
						}
					})

					
					// salas
					angular.forEach(item.sessions, function(session) {
						var d = moment(session.date);
						
						var months = ['jan', 'fev', 'mar', 'abr', 'mai', 'jun', 'jul', 'ago', 'set', 'out', 'nov', 'dez'];
						var day = d.date();
						var month = months[d.month()];
						var hour = d.hour();
						var minutes = d.minute();
						var today = moment();
						var idDate = day +  '-' + month;
						var idRoom = session.room;
						
						// filtra as sessões que ja passaram
						var past = d.isBefore(today);
						
						if(hour < 10) {
							hour = '0' + hour;
						}
						
						if(minutes < 10) {
							minutes = '0' + minutes;
						}
						
						session.hour = hour;
						session.hour_text = hour + ':' + minutes;
						session.past = past;

						// cria o dia
						if(!sessionsByDate[idDate]) {
							sessionsByDate[idDate] = {
								title: day + '/' + month,
								day: day,
								month: month,
								rooms: {}
							};
						}
						
						// cria a sala
						if(!sessionsByDate[idDate].rooms[idRoom]) {
							sessionsByDate[idDate].rooms[idRoom] = {
								title: session.room,
								sessions: []
							}
						}
						
						sessionsByDate[idDate].rooms[idRoom].sessions.push(session);
						
						// horarios
						if(itemHours.indexOf(session.hour_text) < 0) {
							itemHours.push(session.hour_text);
						}
						
						if(hours.indexOf(session.hour_text) < 0) {
							hours.push(session.hour_text);
						}

					})

					filteredData[index].hours = itemHours;
					filteredData[index].genres = itemGenres;
					filteredData[index].iframe_video = $filter('trustAsHtml')(iframeVideo);
					filteredData[index].sessions_by_date = sessionsByDate;

				})

				genres.sort(function(a, b) {
					if(a < b) return -1;
					if(a > b) return 1;
					return 0;
				});

				return {
					data: filteredData,
					hours: hours,
					genres: genres
				}

			}
		}


		function filterData() {
			return function(data, title, genre, hour) {

				angular.forEach(data, function(item, index) {
					
					var disabled = false;

					if((title && item.title.toLowerCase().search(title.toLowerCase()) < 0) ||
						(genre && item.genre.search(genre) < 0) || 
						(hour && item.hours.indexOf(hour) < 0)) {
						disabled = true;
					}

					if(disabled) {
						item.disabled = true;
					} else {
						item.disabled = false;
					}
				})

				return data;
			}	
		}
		
		function filterDataByGenre() {
			return function(data, genres, avoid_id) {

				var filteredData = [];

				if(data) {
					
					angular.forEach(data, function(item, index) {
						var result = false;
						angular.forEach(genres, function(genre) {
							if(item.genre.search(genre) >= 0) {
								result = true;
							}
						});
						if(result && item.id != avoid_id) {
							filteredData.push(item);
						}
					})

				}

				return filteredData;
			}	
		}
	
})();
