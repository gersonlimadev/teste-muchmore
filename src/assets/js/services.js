//  ____                  _
// / ___|  ___ _ ____   _(_) ___ ___  ___
// \___ \ / _ \ '__\ \ / / |/ __/ _ \/ __|
//  ___) |  __/ |   \ V /| | (_|  __/\__ \
// |____/ \___|_|    \_/ |_|\___\___||___/

(function(){
	'use strict';
	
	angular.module('MainApp.Services', [])
		.service('ApiService', ApiService)
	
	ApiService.$inject = ['$http', '$q'];
	function ApiService($http, $q) {

		var data;

		var req = function(url, data, callback) {
			$http({
				method: 'GET',
				url: url,
				data: data,
				// headers: {
				// 	'Content-Type': 'application/json; charset=UTF-8',
				// 	'Access-Control-Allow-Origin': '*'
				// },
			}).then(callback)
		}

		this.getData = function() {
			var defer = $q.defer();
			if(data) {
				return data;
			} else {
				req(CONFIG.baseUrl + '/data/api.json', {}, function(response) {
					defer.resolve(response)
				});
			}
			return data = defer.promise;
		}
		
	}


})();