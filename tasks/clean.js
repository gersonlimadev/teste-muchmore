const gulp = require('gulp')
const paths = require('../paths')
const clean = require('gulp-clean')


gulp.task('clean', function() {
	var files = [
		`${paths.scripts.dist}/app_vendors.js`,
		`${paths.styles.dist}/style_vendors.css`
	];
	return gulp
			.src(files, {read: false})
			.pipe(clean());
});

gulp.task('clean-css', function() {
	return gulp
			.src(`${paths.styles.dist}/style_vendors.css`, {read: false})
			.pipe(clean());
});

gulp.task('clean-js', function() {
	var files = [
		`${paths.scripts.dist}/app_vendors.js`
	];
	return gulp
			.src(files, {read: false})
			.pipe(clean());
});