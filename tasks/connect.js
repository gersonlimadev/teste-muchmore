const gulp = require('gulp')
const paths = require('../paths')
const connect = require('gulp-connect')
const notify = require('gulp-notify')
const util = require('gulp-util')
const livereload = require('gulp-livereload')

gulp.task('connect', function() {
    connect.server({
        root: './public/',
        port: process.env.PORT || 5000,
        livereload: true
    });

});