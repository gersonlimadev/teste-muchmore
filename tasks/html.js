const gulp = require('gulp')
const paths = require('../paths')
const htmlmin = require('gulp-htmlmin')
const notify = require('gulp-notify')
const util = require('gulp-util')
const livereload = require('gulp-livereload')

gulp.task('html', function() {

	var files = [
		`${paths.html.src}/*.html`
	];

	return gulp
			.src(files)
			.pipe(htmlmin({
				collapseWhitespace: true
			}))
			.pipe(gulp.dest(`${paths.html.dist}/`))
			.pipe(global.isWatching ? notify('HTML combined!') : util.noop())
			.pipe(livereload())

});