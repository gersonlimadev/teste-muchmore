const gulp = require('gulp')

const paths = require('../paths')
const babelify = require('babelify')
const browserify = require('browserify')
const uglify = require('gulp-uglify')
const notify = require('gulp-notify')

const concat = require('gulp-concat')
const source = require('vinyl-source-stream')
const buffer = require('vinyl-buffer')
const util = require('gulp-util')

const livereload = require('gulp-livereload')


gulp.task('scripts_vendors', () => {

	var scripts_vendors = [
		`${paths.vendors.src}/angular/angular.min.js`,
		`${paths.vendors.src}/angular/angular-route.min.js`,
		`${paths.vendors.src}/momentjs/moment.min.js`
	];

	return gulp
		.src(scripts_vendors)
		.pipe(concat('app_vendors.js'))
		.pipe(gulp.dest(`${paths.scripts.dist}/`))

});

gulp.task('scripts', ['scripts_vendors'], () => {

	var files = [
		`${paths.scripts.dist}/app_vendors.js`,
		`${paths.scripts.src}/app.js`,
		`${paths.scripts.src}/controllers.js`,
		`${paths.scripts.src}/services.js`,
		`${paths.scripts.src}/directives.js`,
		`${paths.scripts.src}/filters.js`
	];

	return gulp
		.src(files)
		.pipe(concat('scripts.combined.js'))
		.pipe(gulp.dest(`${paths.scripts.dist}/`))
		.pipe(global.isWatching ? notify('Scripts combined!') : util.noop())
		.pipe(livereload())

});