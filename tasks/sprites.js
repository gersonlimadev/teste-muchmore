const gulp = require('gulp')
const paths = require('../paths')
const util = require('gulp-util')
const spritesmith = require('gulp.spritesmith')
const livereload = require('gulp-livereload')

gulp.task('sprites', function() {

	var spriteData = gulp.src(`${paths.images.src}/*.png`).pipe(spritesmith({
		cssFormat: 'stylus',
		algorithm: 'binary-tree',
		padding: 2,
		imgPath: 'assets/img/sprite.png',
		imgName: 'sprite.png',
		cssName: 'sprite.styl',
		retinaImgName: 'sprite@2x.png',
		retinaSrcFilter: [`${paths.images.src}/*@2x.png`],
	}));

	spriteData.img.pipe(gulp.dest(`${paths.images.dist}/`));
	spriteData.css.pipe(gulp.dest(`${paths.styles.src}/utils/`));

});