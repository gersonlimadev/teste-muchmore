const gulp = require('gulp')
const paths = require('../paths')
const stylus = require('gulp-stylus')
const nib = require('nib')
const notify = require('gulp-notify')
const util = require('gulp-util')
const concat = require('gulp-concat')
const livereload = require('gulp-livereload')

gulp.task('styles_app', () => {

	return gulp
		.src(`${paths.styles.src}/style.styl`)
		.pipe(stylus({
			compress: true,
			use: nib()
		}))
		.pipe(gulp.dest(`${paths.styles.dist}/`))

});

gulp.task('styles_vendors', () => {

	var vendors = `${paths.vendors.src}/**/*.css`;

	return gulp
		.src(vendors)
		.pipe(concat('style_vendors.css'))
		.pipe(gulp.dest(`${paths.styles.dist}/`))

});

gulp.task('styles', ['styles_app', 'styles_vendors'], () => {

	var files = [
		`${paths.styles.dist}/style_vendors.css`,
		`${paths.styles.dist}/style.css`
	];

	return gulp
		.src(files)
		.pipe(concat('style.css'))
		.pipe(gulp.dest(`${paths.styles.dist}/`))
		.pipe(global.isWatching ? notify('Styles combined!') : util.noop())
		.pipe(livereload())

});