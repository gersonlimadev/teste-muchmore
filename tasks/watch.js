const gulp = require('gulp')
const paths = require('../paths')
const watch = require('gulp-watch')
const livereload = require('gulp-livereload')
const sequence = require('run-sequence').use(gulp)

gulp.task('watch', ['build'], function() {

	global.isWatching = true;

	livereload.listen();

	gulp.watch(`${paths.html.src}/*.html`, () => {
		sequence('html')
	});

	gulp.watch(`${paths.images.src}/*.png`, () => {
		sequence('sprites', 'styles', 'clean-css')
	});

	gulp.watch([`${paths.styles.src}/**/*.styl`, `${paths.vendors.src}/**/*.css`], () => {
		sequence('styles', 'clean-css')
	});

	gulp.watch([`${paths.scripts.src}/**/*.js`, `${paths.vendors.src}/**/*.js`], () => {
		sequence('scripts', 'clean-js')
	});

});